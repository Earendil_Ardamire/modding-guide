This is the GitLab for the "Mount & Blade Language and Engine Documentation", replacing the former "Modding Guide for M&B Warband".
It is not written up via Latex anymore but via html. This should easier the access for potential helpers and also make the documentation easier accessible compared to scrolling through a nearly 300 page long pdf document.
Everyone can request access to it, so everyone can bring in some bits of knowledge.

If you want to work some bits in, simply add a new branch at GitLab, make your changes and commit them. They will then get reviewed and (potentially) integrated into the Documentation.

This html Documentation is set up such way that modders can download it and access it later without the need to have an internet connection.

Thanks for helping ^^
